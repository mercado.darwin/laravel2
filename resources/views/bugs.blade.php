@extends("layouts.app")
@section("content")

<h1 class="text-center py-5">ALL BUGS</h1>
<div class="container">
<div class="row">
@foreach($bugs as $indiv_bug)
<div class="col-lg-4 my-2">
<div class="card">
<div class="card-body">
<h4 class="card-title">{{$indiv_bug->title}}</h4>
<p class="card-text">{{$indiv_bug->body}}</p>
<p class="card-text">{{$indiv_bug->category_id}}</p>
<p class="card-text">{{$indiv_bug->status_id}}</p>
<p class="card-text">{{$indiv_bug->user_id}}</p>
</div>
</div>
</div>
@endforeach
</div>
</div>

@endsection